Supported data formats
=======================

Please use the DataReader.supported_data_formats() class method to get the most recent list of 
supported data formats.

.. code-block:: python

  DataReader.supported_data_formats()


**Table: suported data formats**

=========== ============ ==========
Method      file format  sub-format
=========== ============ ==========
Absorbance  BMG omega    uv-vis
----------- ------------ ----------
   Abs       JASCO        uv-vis
=========== ============ ==========

