README
========

**LabDataReader** is a plugin based python framework for reading and converting 
laboratory device data, including metadata and semantic information.
As input formats many widely used (legacy) data formats are supported or can be very 
easily added through the plugin system.
Supported output formats are Pandas Dataframes, JSON and csv.
**AnIML** output is supported via the AnIML_python package.

Features:
  * standardised output data formats and interfaces 
  * metadata parsing and handling, uniform and flexible metadata class
  * semantic information in human and machine readable formats
  * convenient methods for parsing of legacy measurement data

The motivation of the library is to make data reading of lab device files as simple as possible. For that a generic description of data formats has been developed:

LabDataReader destinguishes between
  - **methods**:
     like "HPLC", "Absorbance", "GC", "NMR",...
  - **data formats**: 
    these are vendor/measurement program specific output formats,
    like "hitachi_chromaster", "thermo_skanit6", "bmg_omega" 
  - **data sub formats**:
    each data format can have several sub-formats, 
    like "chromatogram_uv_vis", "uv_vis_kinetics", which can be used to further specify different 
    variations of a data format.

  By this addressing scheme all supported data formats can be addressed. 
  There are also methods provided to query all supported data formats with their corresponding sub formats.

Furthermore LabDataReader provides semantic description (RDF/turtle) of the imported data formats to relate the data to ontologies.
