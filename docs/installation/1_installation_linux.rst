LabDataReader installation on Linux
_____________________________________

Installation of sila 2 can be as simple as writing:

.. code:: bash

    pip3 install labdatareader
    
But it is highly recommended to do this in a virtual environment.
