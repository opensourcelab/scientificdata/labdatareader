labdatareader.methods.absorbance.generic package
================================================

Submodules
----------

labdatareader.methods.absorbance.generic.csv module
---------------------------------------------------

.. automodule:: labdatareader.methods.absorbance.generic.csv
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: labdatareader.methods.absorbance.generic
   :members:
   :undoc-members:
   :show-inheritance:
