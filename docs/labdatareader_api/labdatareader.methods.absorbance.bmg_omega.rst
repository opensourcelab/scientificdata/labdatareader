labdatareader.methods.absorbance.bmg\_omega package
===================================================

Submodules
----------

labdatareader.methods.absorbance.bmg\_omega.uv\_vis module
----------------------------------------------------------

.. automodule:: labdatareader.methods.absorbance.bmg_omega.uv_vis
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: labdatareader.methods.absorbance.bmg_omega
   :members:
   :undoc-members:
   :show-inheritance:
