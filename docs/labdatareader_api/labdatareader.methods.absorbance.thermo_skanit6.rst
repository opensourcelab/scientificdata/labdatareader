labdatareader.methods.absorbance.thermo\_skanit6 package
========================================================

Submodules
----------

labdatareader.methods.absorbance.thermo\_skanit6.uv\_vis module
---------------------------------------------------------------

.. automodule:: labdatareader.methods.absorbance.thermo_skanit6.uv_vis
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: labdatareader.methods.absorbance.thermo_skanit6
   :members:
   :undoc-members:
   :show-inheritance:
