labdatareader.methods.absorbance.jasco package
==============================================

Submodules
----------

labdatareader.methods.absorbance.jasco.uv\_vis\_kinetics module
---------------------------------------------------------------

.. automodule:: labdatareader.methods.absorbance.jasco.uv_vis_kinetics
   :members:
   :undoc-members:
   :show-inheritance:

labdatareader.methods.absorbance.jasco.uv\_vis\_singlepoint module
------------------------------------------------------------------

.. automodule:: labdatareader.methods.absorbance.jasco.uv_vis_singlepoint
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: labdatareader.methods.absorbance.jasco
   :members:
   :undoc-members:
   :show-inheritance:
