Development
=============

:doc:`1_contributing_LabDataReader`

:doc:`2_issues`
:doc:`3_merge_requests`
:doc:`4_coding_style_guidelines`
:doc:`5_writing_data_reader_plugins`

:doc:`9_build_package`
:doc:`10_git-workflow`
:doc:`11_build_documentation`


