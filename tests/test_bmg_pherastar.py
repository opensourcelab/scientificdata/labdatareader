"""_____________________________________________________________________

:PROJECT: LabDataReader

*Test BMG pherastar data importer*

:details: Test BMG pherastar data importer

:authors: mark doerr (mark@uni-greifswald.de), 
          till becker (till.becker@stud.tu-darmstadt.de)

:date: (creation)          20210116

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from labdatareader.data_reader import DataReader


class BMGPherastarTestCase(unittest.TestCase):
    def setUp(self):
        # print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'test_data', 'absorbance', 'bmg_pherastar')

    def test_read_metadata(self):
        self.abs_filename_pattern = "BMG_MTP00657_*.csv"
        self.bmg_pherastar_data_reader = DataReader(method='absorbance',
                                                    data_format="bmg_pherastar.uv_vis",
                                                    data_path=self.data_path,
                                                    filename_pattern=self.abs_filename_pattern)
        pherastar_meta = self.bmg_pherastar_data_reader.metadata

        print("bmg pherastar core md:", pherastar_meta.list_core())

        print("bmg pherastar test-time:",
              self.bmg_pherastar_data_reader.describe_data("datetime"))

        for key, val in pherastar_meta.items():
            print(key, val)

        logging.debug(pherastar_meta.device_version)

        self.assertEqual(pherastar_meta.method, 'absorbance')
        self.assertEqual(pherastar_meta.device_type, 'microtiter platereader')
        self.assertEqual(pherastar_meta.meas_software, 'PHERAstar FSX')
        #self.assertEqual(omega_meta.meas_software_version, 'ver. 6.0.3.3')

    def test_read_data(self):
        self.abs_filename_pattern = "BMG_MTP00657_*.csv"
        self.bmg_pherastar_data_reader = DataReader(method='absorbance',
                                                    data_format="bmg_pherastar.uv_vis",
                                                    data_path=self.data_path,
                                                    filename_pattern=self.abs_filename_pattern)
        logging.debug(
            f"read xml {self.bmg_pherastar_data_reader.data_file_list}")
        test_filename = "BMG_MTP00657_14.02.2020_11-48-18.csv"
        self.assertEqual(self.bmg_pherastar_data_reader.data_file_list[0],
                         os.path.join(self.data_path, test_filename))

        pherastar_df = self.bmg_pherastar_data_reader.dataframe

        #self.assertEqual(omega_df.loc[0, "value"], 0.0575 )
        #self.assertEqual(omega_df.loc[0, "delta_time_ref_s"], 0.0 )
        #self.assertEqual(omega_df.loc[191, "value"], 0.061 )
        #self.assertEqual(omega_df.loc[191, "delta_time_ref_s"], 0.0 )

    def test_read_data_decoding(self):
        logging.debug(f"testing if decoding works correctly")
        self.abs_filename_pattern = "BMG_MTP00657_*.csv"
        self.bmg_pherastar_data_reader = DataReader(method='absorbance',
                                                    data_format="bmg_pherastar.uv_vis",
                                                    data_path=self.data_path,
                                                    filename_pattern=self.abs_filename_pattern)

        pherastar_df = self.bmg_pherastar_data_reader.dataframe
        # print(pherastar_df)

        #self.assertEqual(omega_df.loc[0, "value"], 0.0575 )
        #self.assertEqual(omega_df.loc[0, "delta_time_ref_s"], 0.0 )
        #self.assertEqual(omega_df.loc[191, "value"], 0.061 )
        #self.assertEqual(omega_df.loc[191, "delta_time_ref_s"], 0.0 )


if __name__ == '__main__':
    logging.basicConfig(
        format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
