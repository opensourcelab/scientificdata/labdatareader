"""_____________________________________________________________________

:PROJECT: LabDataReader

*Test JASCO absorbance csv data importer*

:details: Test JASCO absorbance csv data importer

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20210206

.. note:: -
.. todo:: -

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from labdatareader.data_reader import DataReader
from labdatareader.data_descriptor import DescriptionFormat as ddft


class GenericAbsorbanceTestCase(unittest.TestCase):
    def setUp(self):
        # print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'test_data', 'absorbance', 'jasco')
        self.header_map = {'t': 'time', 'abs': 'value'}
        self.default_metadata = {"barcode": "BC_42",
                                 "environment_temperature": 305}
        self.default_data = {"wavelength": 666}

    def test_read_metadata(self):
        self.abs_filename_pattern = "201009_pNPP_60um.csv"

        self.generic_data_reader = DataReader(method='absorbance',
                                              data_format="jasco.uv_vis_kinetics",
                                              data_path=self.data_path,
                                              filename_pattern=self.abs_filename_pattern,
                                              header_map=self.header_map
                                              )
        generic_meta = self.generic_data_reader.metadata

        # print("generic core md:", generic_meta.list_core())
        # print("generic test-time:",
        #      self.generic_data_reader.describe_data("datetime"))

        # for key, val in generic_meta.items():
        #    print(key, val)

        logging.debug(generic_meta.device_version)

        self.assertEqual(generic_meta.method, 'absorbance')
        self.assertEqual(generic_meta.device_type, 'spectrophotometer')
        self.assertEqual(generic_meta.meas_software, 'JASCO')

        self.assertEqual(generic_meta.default_visualisation, 'plot2D.xy')

    def test_read_data(self):
        self.abs_filename_pattern = "201009_pNPP_60um.csv"
        self.generic_data_reader = DataReader(method='absorbance',
                                              data_format="jasco.uv_vis_kinetics",
                                              data_path=self.data_path,
                                              filename_pattern=self.abs_filename_pattern,
                                              header_map=self.header_map,
                                              data_start_ends=[(0, 12)]
                                              )
        # logging.debug(
        #    f"read generic {self.generic_data_reader.data_file_list}")
        test_filename = "201009_pNPP_60um.csv"
        self.assertEqual(self.generic_data_reader.data_file_list[0],
                         os.path.join(self.data_path, test_filename))

        generic_df = self.generic_data_reader.dataframe

        self.assertEqual(self.generic_data_reader.describe_data(
            "value"), 'absorption value measured')
        self.assertEqual(self.generic_data_reader.describe_data(
            "value", descr_format=ddft.QUANT), 'absorbance')

        self.assertAlmostEqual(generic_df.loc[0, "value"], -0.08554)
        # default value test
        self.assertEqual(generic_df.loc[0, "well_name"], "A01")

        # self.assertEqual(generic_df.loc[0, "delta_time_ref_s"], 0.0)
        # self.assertEqual(generic_df.loc[191, "delta_time_ref_s"], 0.0 )


if __name__ == '__main__':
    # logging.basicConfig(
    #    format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    logging.basicConfig(
        format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
