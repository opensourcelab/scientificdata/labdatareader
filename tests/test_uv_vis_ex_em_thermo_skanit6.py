"""_____________________________________________________________________

:PROJECT: LabDataReader

*Thermo SkanIt6 data importer*

:details: Thermo SkanIt6 xml data importer fluorescence

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20220318

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.3"

import os
import unittest
import logging

from labdatareader.data_reader import DataReader


class ThermoSkanit6TestCase(unittest.TestCase):
    def setUp(self):
        # print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'test_data',  'fluorescence', 'thermo_skanit6')
        self.abs_filename_pattern = "*SPfl96_488-514*.xml"
        self.thermo_skanit6_data_reader = DataReader(method='fluorescence',
                                                     data_format="thermo_skanit6.uv_vis_ex_em",
                                                     data_path=self.data_path,
                                                     filename_pattern=self.abs_filename_pattern)

    def test_read_metadata(self):
        sk6_meta = self.thermo_skanit6_data_reader.metadata

        self.assertEqual(sk6_meta.method, 'fluorescence')
        self.assertEqual(sk6_meta.device_type, 'microtiter platereader')
        self.assertEqual(sk6_meta.meas_software, 'AiServer RE')
        self.assertEqual(sk6_meta.meas_software_version, 'ver. 6.1.1.7')

    def test_read_dataframe(self):
        #logging.debug(f"read xml {self.thermo_skanit6_data_reader.data_file_list}")
        test_filename = "TA0041_20200622_190639_varioskanLux_SPabs96_600_660_E_P_incubation_cycle_growth_fluoresc.xml"
        # self.assertEqual(self.thermo_skanit6_data_reader.data_file_list[0],
        #                  os.path.join(self.data_path, test_filename))

        skit6_df = self.thermo_skanit6_data_reader.dataframe

        # self.assertEqual(skit6_df.loc[0, "value"], 0.0575)
        # self.assertEqual(skit6_df.loc[0, "delta_time_ref_s"], 0.0)
        # self.assertEqual(skit6_df.loc[191, "value"], 0.061)
        # self.assertEqual(skit6_df.loc[191, "delta_time_ref_s"], 0.0)


if __name__ == '__main__':
    logging.basicConfig(
        format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
