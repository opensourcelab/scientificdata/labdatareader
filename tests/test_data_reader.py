"""_____________________________________________________________________

:PROJECT: LabDataReader

*DataReader Testsuite*

:details: DataReader Testsuite

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201022

.. note:: -
.. todo:: -

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from labdatareader.data_reader import DataReader


class DataReaderCase(unittest.TestCase):
    def setUp(self):
        # logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
        # print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'test_data', 'absorbance', 'generic')
        # self.abs_filename_pattern = "*SPabs96_600_660*.xml"

    def test_register_all_data_formats(self):
        dr = DataReader()

        # print(dr.supported_methods)
        # print(dr.supported_data_format_classes)
        # print(dr.supported_data_format_subclasses)
        # print(dr.supported_data_formats)

        self.assertIn('absorbance', dr.supported_methods)
        self.assertIn('generic', dr.supported_data_format_classes)
        self.assertIn('csv', dr.supported_data_format_subclasses)
        self.assertIn('absorbance.generic.csv', dr.supported_data_formats)

    def test_read_data_df(self):
        # "bmg_omega.uv_vis_singlepoint"
        dr = DataReader(method='absorbance', data_format="generic.csv",
                        data_path=self.data_path, filename_pattern="*.csv")
        # print(dr.dataframe)
        # print(dr.metadata.list_core())

    def test_descriptors(self):
        dr = DataReader()
        self.assertIn('spectroscopic', dr.describe_data_format(
            "absorbance.generic.csv"))


if __name__ == '__main__':
    logging.basicConfig(
        format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    # ~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
