"""_____________________________________________________________________

:PROJECT: LabDataReader

*Test generic absorbance csv data importer*

:details: Test generic absorbance csv data importer

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20210122

.. note:: -
.. todo:: -

________________________________________________________________________
"""

__version__ = "0.0.2"

import os
import unittest
import logging

from labdatareader.data_reader import DataReader
from labdatareader.data_descriptor import DescriptionFormat as ddft


class GenericAbsorbanceTestCase(unittest.TestCase):
    def setUp(self):
        # print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'test_data', 'absorbance', 'generic')
        self.header_map = {'t': 'time', 'abs': 'value'}
        self.default_metadata = {"barcode": "BC_42",
                                 "environment_temperature": 305}
        self.default_data = {"wavelength": 666}

    def test_read_metadata(self):
        self.abs_filename_pattern = "201009_pNPP_60um.csv"

        self.generic_data_reader = DataReader(method='absorbance',
                                              data_format="generic.csv",
                                              data_path=self.data_path,
                                              filename_pattern=self.abs_filename_pattern,
                                              header_map=self.header_map
                                              )
        generic_meta = self.generic_data_reader.metadata

        # print("generic core md:", generic_meta.list_core())
        # print("generic test-time:",
        #      self.generic_data_reader.describe_data("datetime"))

        # for key, val in generic_meta.items():
        #    print(key, val)

        logging.debug(generic_meta.device_version)

        self.assertEqual(generic_meta.method, 'absorbance')
        self.assertEqual(generic_meta.device_type, 'generic')
        self.assertEqual(generic_meta.meas_software, 'generic-csv')

        self.assertEqual(generic_meta.default_visualisation, 'plot2D.xy')

    def test_read_data(self):
        self.abs_filename_pattern = "201009_pNPP_60um.csv"
        self.generic_data_reader = DataReader(method='absorbance',
                                              data_format="generic.csv",
                                              data_path=self.data_path,
                                              filename_pattern=self.abs_filename_pattern,
                                              header_map=self.header_map,
                                              data_start_ends=[(0, 12)]
                                              )
        # logging.debug(
        #    f"read generic {self.generic_data_reader.data_file_list}")
        test_filename = "201009_pNPP_60um.csv"
        self.assertEqual(self.generic_data_reader.data_file_list[0],
                         os.path.join(self.data_path, test_filename))

        generic_df = self.generic_data_reader.dataframe

        self.assertEqual(self.generic_data_reader.describe_data(
            "value"), 'absorption value measured')
        self.assertEqual(self.generic_data_reader.describe_data(
            "value", descr_format=ddft.QUANT), 'absorbance')

        self.assertAlmostEqual(generic_df.loc[0, "value"], -0.08554)
        # default value test
        self.assertEqual(generic_df.loc[0, "well_name"], "A01")

        # self.assertEqual(generic_df.loc[0, "delta_time_ref_s"], 0.0)
        # self.assertEqual(generic_df.loc[191, "delta_time_ref_s"], 0.0 )

    def test_read_file_list(self):
        gdr = DataReader(method='absorbance', data_format="generic.csv",
                         data_path=self.data_path,
                         header_map=self.header_map,
                         metadata_defaults=self.default_metadata,
                         data_defaults=self.default_data,
                         data_start_ends=[(4, 14)]
                         )
        gdr.set_filename_list(['201009_pNPP_60um_comments_1.csv',
                               '201009_pNPP_60um_comments_3.csv'])
        generic_df = gdr.dataframe
        self.assertEqual(generic_df.loc[19, "value"], -0.07750)

        self.assertIn("CSV Format", gdr.default_device_settings)

    def test_read_blocks(self):
        gdr = DataReader(method='absorbance', data_format="generic.csv",
                         data_path=self.data_path,
                         header_map=self.header_map,
                         metadata_defaults=self.default_metadata,
                         data_defaults=self.default_data,
                         data_start_ends=[(4, 14), (16, 26), (28, 38)]
                         )
        gdr.set_filename_list(['201009_pNPP_60um_comments_multi_blocks.csv'])

        generic_df = gdr.dataframe
        self.assertEqual(generic_df.loc[29, "value"], -0.0775)

        self.assertIn("CSV Format", gdr.default_device_settings)

    def test_csv_delimiter(self):
        gdr = DataReader(method='absorbance', data_format="generic.csv",
                         data_path=self.data_path,
                         header_map=self.header_map,
                         metadata_defaults=self.default_metadata,
                         data_defaults=self.default_data,
                         data_start_ends=[(4, 14), (16, 26), (28, 38)]
                         )

        gdr.set_filename_list(
            ['201009_pNPP_60um_comments_multi_blocks_semicolon.csv'])
        gdr.csv_delimiter = ';'

        generic_df = gdr.dataframe
        self.assertEqual(generic_df.loc[29, "value"], -0.0775)


if __name__ == '__main__':
    # logging.basicConfig(
    #    format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    logging.basicConfig(
        format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
