"""_____________________________________________________________________

:PROJECT: LabDataReader

*geolocation and weather*

:details: geolocation and weather.
         see
         https://towardsdatascience.com/pythons-geocoding-convert-a-list-of-addresses-into-a-map-f522ef513fd6
         https://geocoder.readthedocs.io/providers/OpenStreetMap.html
         http://osmapi.metaodi.ch/


:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20210123

.. note:: -
.. todo:: - conversion RDF-turtle in RDF-XML

________________________________________________________________________

"""

__version__ = "0.0.1"

from enum import Enum
import logging
import json
from typing import Optional

from geopy.geocoders import Nominatim


def main():
    """
    docstring
    """


if __name__ == "__main__":
    geolocator = Nominatim(user_agent="sample app")
    output_file = "geo_metadata.json"

    #address = "1 Apple Park Way, Cupertino, CA"
    address = "Felix-Hausdorff Straße 4, Greifswald, Germany"
    data = geolocator.geocode(address)

    print(data.raw.get("lat"), data.raw.get("lon"))

    print(data.point)

    print(data.point.latitude, data.point.longitude, data.point.altitude)

    geoloc_dict = {"geo-location": [data.point.latitude, data.point.longitude],
                   "altitude": data.point.altitude}

    with open(output_file, 'w') as json_file:
        json.dump(geoloc_dict, json_file)
