"""_____________________________________________________________________

:PROJECT: LabDataReader

*metadata descriptor base class*

:details: metadata descriptor base class.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20210122

.. note:: -
.. todo:: - conversion RDF-turtle in RDF-XML

________________________________________________________________________

"""

__version__ = "0.0.1"

import numpy as np

from labdatareader.data_descriptor import DataDescriptor
from labdatareader.data_descriptor import DescriptionFormat as desf


class MetaDataDescriptor(DataDescriptor):

    description_md = "This is the Metadata ..."

    rdf_header = "RDF header .. to be implemented"

    standard_items = {"timestamp": {"markdown": "",
                                    "default": np.nan,  # time 0
                                    "quantity": "timepoint",
                                    "dimension": "time",
                                    "unit": "date-time",
                                    "type": "",
                                    "RDF-turtle": "LabDataReader:"},
                      "method": {"markdown": "name of the physical method",
                                 "default": "generi method",
                                 "RDF-turtle": "LabDataReader:"},
                      "meas_procedure_name": {"markdown": "name of the measurement procedure",
                                              "default": "generic procedure",
                                              "RDF-turtle": "LabDataReader:"},
                      "session_name": {"markdown": "name of measurement session - user defined",
                                       "default": "generic session",
                                       "RDF-turtle": "LabDataReader:"},
                      "meas_software": {"markdown": "name of the software with which the measurement was taken",
                                        "default": "unknown measurement software",
                                        "RDF-turtle": "LabDataReader:"},
                      "meas_software_version": {"markdown": "version of the software with which the measuremt was taken",
                                                "default": __version__,
                                                "RDF-turtle": "LabDataReader:"},
                      "device_type": {"markdown": "type / class of the device, e.g. HPLC, GC",
                                      "default": "generic device type",
                                      "RDF-turtle": "LabDataReader:"},
                      "device_name": {"markdown": "name of the device",
                                      "default": "generic device",
                                      "RDF-turtle": "LabDataReader:"},
                      "device_version": {"markdown": "version of the device",
                                         "default": __version__,
                                         "RDF-turtle": "LabDataReader:"},
                      "device_serial": {"markdown": "serial number of the measurement device",
                                        "default": "no serial",
                                        "RDF-turtle": "LabDataReader:"},
                      "environment_temperature": {"markdown": "temperature of the environment / laboratory",
                                                  "type": float,
                                                  "default": np.nan,
                                                  "quantity": "temperature",
                                                  "dimension": "temperature",
                                                  "unit": "K",
                                                  "RDF-turtle": "LabDataReader:"},
                      "environment_air_pressure": {"markdown": "air pressure of the environment / laboratory",
                                                   "type": float,
                                                   "default": np.nan,
                                                   "quantity": "pressure",
                                                   "dimension": "force/area",
                                                   "unit": "N/m^2",
                                                   "RDF-turtle": "LabDataReader:"},
                      "environment_air_humidity": {"markdown": "air humidty of the environment / laboratory",
                                                   "type": float,
                                                   "default": np.nan,
                                                   "unit": "/1",
                                                   "RDF-turtle": "LabDataReader:"},
                      "users": {"markdown": "users involved in the measurement",
                                "default": "generic users",
                                "RDF-turtle": "LabDataReader:"},
                      "geolocation": {"markdown": "geological location where the measurement was done",
                                      "default": [],
                                      "unit": "",
                                      "RDF-turtle": "LabDataReader:"},
                      "altitude": {"markdown": "geological altitude of the location at which the measurement was done",
                                   "type": float,
                                   "default": np.nan,
                                   "unit": "m",
                                   "RDF-turtle": "LabDataReader:"},
                      }

    def description(self, descr_format: desf = desf.MD):
        """
        docstring
        """
        if descr_format is desf.MD:
            return self.description_md
        else:
            raise NotImplementedError

    def description_method(self, descr_format: desf = desf.MD):
        pass

    def description_device_settings(self, descr_format: desf = desf.MD):
        pass
