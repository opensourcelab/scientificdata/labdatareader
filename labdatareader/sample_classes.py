"""_____________________________________________________________________

:PROJECT: LabDataReader

*sample classes factors*

:details: sample classes factors.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)       20201026

.. note:: -
.. todo:: - use it !

________________________________________________________________________

"""

__version__ = "0.0.1"

from enum import Enum


class SampleClass(Enum):
    sample = 'S'
    reference = 'R'
    posCtrl = 'pCTRL'
    negCtrl = 'nCTRL'
    blank = 'BL'
    empty = '-'
