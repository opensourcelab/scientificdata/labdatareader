"""_____________________________________________________________________

:PROJECT: LabDataReader

*data_reader meta data*

:details: data_reader meta data.

:authors: mark doerr (mark@uni-greifswald.de)
          Stefan Maak

:date: (creation)          20201021

.. note:: -
.. todo:: - descriptors

________________________________________________________________________

"""

__version__ = "0.0.4"

import sys
from typing import Union
from enum import Enum, auto
import collections

from rdflib import Graph, Namespace, URIRef, BNode, Literal
from rdflib import RDF, RDFS, OWL, SKOS, DC, FOAF, XSD

from labdatareader.data_descriptor import DataDescriptor
from labdatareader.metadata_descriptor import MetaDataDescriptor


class Plot2D(Enum):
    XY = "plot2D.xy"
    BOXPLOT = "plot2D.boxplot"
    TOPN = "plot2D.topN"
    HEATMAP = "plot2D.heatmap"
    CHROMATOGRAM = "plot2D.chromatogram"
    GROWTHCURVE = "plot2D.growthcurve"


class Plot3D(Enum):
    XYZ = "plot3D.xyz"
    HEATMAP = "plot3D.heatmap"
    BARPLOT = "plot3D.barplot"


class MetaDataCore(collections.abc.Mapping):
    """Core Metadata - specifies attributes all metadata should have
    Units: all measured values shall be in SI units.

    The followining core attributes are recommended to be provided as metadata to all data:
    timestamp :     datetime - date and time (in ISO format) when measurement started
    method:         str - measurement method (e.g. HPLC, GC, NMR, Absorbance, ...)
    meas_procedure_name: str - name of the measurement procedure (name of the general sequence measurement steps)
    session_name:   str - name of the measurement session where the measurement was taken
    device_type:    str - type of the measurement device (e.g. UV-Vis spectrometer, )
    device_name:    str -full name of the device, including the vendor name
    device_version: str -version of the device
    device_serial:  str - serial number of the device
    meas_software:  str - name of the measurement software
    meas_software_version: str - version of the measurement software
    environment_temperature: float - average environmental temperature across the measurement in K
    environment_air_pressure: float - average environmental air pressure across the measurement in N/m^2
    environment_air_humidity: float - average environmental air humidity ratio across the measurement in kg/kg
    users:           list - list of users measureing the data
    geolocation:    str  - location of the measurement
    altitude:       float - altitude at which the measurement was taken in m

    :raises MetaDataIncompleteError: This error is raised when some core metadata is missing
    :return: [description]
    :rtype: [type]
    :yield: [description]
    :rtype: [type]
    """

    __slots__ = 'timestamp', \
                'method', 'meas_procedure_name', 'session_name', \
                'meas_software', 'meas_software_version',\
                'device_type', 'device_name',  \
                'device_version', 'device_serial',\
                'environment_temperature',\
                'environment_air_pressure',\
                'environment_air_humidity',\
                'users', 'geolocation', 'altitude'

    # average environmental temperature across the measurement in K
    environment_temperature: float
    # average environmental air pressure across the measurement in N/m^2
    environment_air_pressure: float
    # average environmental air humidity ratio across the measurement in kg/kg
    environment_air_humidity: float

    def __contains__(self, item):
        return item in self.__slots__

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)

    def __len__(self):
        return len(self.__slots__)

    def __iter__(self):
        return iter(self.__slots__)

    def items(self):
        try:
            for attribute in self.__slots__:
                yield attribute, getattr(self, attribute)
        except AttributeError as err:
            raise MetaDataIncompleteError(err)


class MetaData:
    """This class handles all meta-data, like measurement device, datetime, user, etc.
     Units: all measured values shall be in SI units

    :todo: container geometry

    :raises MetaDataIncompleteError: [description]
    :return: [description]
    :rtype: [type]
    :yield: [description]
    :rtype: [type]
    """
    # some common metadata
    meas_filenames: list
    sample_id: str
    barcode: str
    num_cols: int
    num_rows: int
    session_description: str
    timestamps: list
    meas_temperatures: list
    wavelengths: list
    visualisation_default: Union[Plot2D, Plot3D]
    # recommendation for visualisations
    visualisations: [Union[Plot2D, Plot3D]]

    def __init__(self):
        """
        generic meta data init
        """
        self.attributes = set({})
        self.core = MetaDataCore()

        self.descriptor = MetaDataDescriptor()

    def __setitem__(self, key, value):
        if key in self.core:
            self.core.__setitem__(key, value)
        else:
            setattr(self, key, value)
            self.attributes.add(key)

    def __iter__(self):
        return iter(dir(self))

    def items(self):
        """get iterator for all items

        :raises MetaDataIncompleteError: [description]
        :yield: attribute
        :rtype: attribute type
        """
        try:
            for attribute in self.attributes:
                yield attribute, getattr(self, attribute)
        except AttributeError as err:
            raise MetaDataIncompleteError(err)

    def __len__(self):
        return len(dir(self))

    def __getattr__(self, item):
        if item in ['core', 'attributes']:
            return super().__getattribute__(item)
        if item in self.core:
            return getattr(self.core, item)
        else:
            return super().__getattribute__(item)

    def __setattr__(self, key, value):
        if key in ['core', 'attributes']:
            super().__setattr__(key, value)
        elif key in self.core:
            self.core.__setitem__(key, value)
        else:
            super().__setattr__(key, value)
            self.attributes.add(key)

    def __dir__(self):
        return self.attributes.union(set(self.core.__slots__))

    def __getitem__(self, key):
        if key in list(self.core.__slots__):
            return self.core[key]
        else:
            return getattr(self, key)

    def list_core(self):
        """list core metadata items

        :return: list of core items
        :rtype: list
        """
        return [item for item in self.core.items()]

    @property
    def default_visualisation(self):
        return self.visualisation_default.value

    @property
    def visualisations_list(self):
        return [visualisation.value for visualisation in self.visualisations]

    def gen_graph(self):

        ldr = Namespace(
            "http://lara.uni-greifswald.de/ontologies/2021/1/labdatareader_simple2#")

        self.graph = Graph()
        # self.graph.parse("labdatareader_simple4.ttl", format="ttl")

        self.graph.bind("ldr", ldr)
        self.graph.bind("RDF", RDF)
        self.graph.bind("RDFS", RDFS)
        self.graph.bind("OWL", OWL)

        ldr_ontology = ((ldr.User,  RDF.type,  OWL.Class),
                        (ldr.User, OWL.equivalentClass,  URIRef(
                            "http://xmlns.com/foaf/0.1/Person"))
                        )

        # rdfs: subClassOf ldr: Measurement .
        for triple in ldr_ontology:
            self.graph.add(triple)

        self.graph.add((ldr.USR1, RDF.type, OWL.NamedIndividual))
        self.graph.add((ldr.USR1, RDF.type, ldr.User))
        self.graph.add((ldr.USR1, URIRef(
            "http://xmlns.com/foaf/0.1/firstName"), Literal(self.users[0])))
        # self.graph.add(()) FOAF.firstName

        # <http://xmlns.com/foaf/0.1/firstName> "Benjamin"^^rdfs:Literal ;
        # <http://xmlns.com/foaf/0.1/lastName> "Lear 3"^^rdfs:Literal .

    def query_graph(self, query: str):

        qres = self.graph.query(
            query, initNs={'foaf': FOAF, 'rdf': RDF, 'rdfs': RDFS})

        return qres


class MetaDataIncompleteError(Exception):
    """This exception is thrown, when the metadata core is not complete

    :param Exception: [description]
    :type Exception: [type]
    """

    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return f"MetaDataIncompleteError: Missing attribute: {self.message}"
        else:
            return f"MetaDataIncompleteError: metadata attributes missing."
