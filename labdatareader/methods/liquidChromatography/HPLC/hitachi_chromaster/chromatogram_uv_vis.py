"""_____________________________________________________________________

:PROJECT: LabDataReader

*Hitachi chromaster chromatogram UV-vis  data importer*

:details: Hitachi chromaster chromatogram UV-vis  data importer

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201208

.. note:: -
.. todo:: - meta data
          - temperature
          - num readings (for spectrum / kinetics)

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import sys
import csv
import re
import logging
from datetime import datetime, timedelta

import pandas as pd
import numpy as np

from labdatareader.metadata import MetaData
from labdatareader.data_reader_implementation import DataReaderImplementation


class DataReaderImpl(DataReaderImplementation):
    def __init__(self, data_path: str = ".",
                 filename_pattern: str = "*.txt"):
        super().__init__(data_path=data_path,
                         filename_pattern=filename_pattern)

        self.data_start_line = None

    def _read_all_data(self):
        #data_lst = []
        # currently only one file is supported
        # decide to read one single file or many in list
        # for data_filename in self.data_file_list:
        data_filename = self.data_file_list[0]

        self._read_meta_data(data_filename)
        self._read_data()

    def _read_meta_data(self, data_filename: str):
        self.metadata_ = MetaData()
        with open(data_filename, 'r') as f_in:
            self.content_ascii_lines = f_in.readlines()

        # extract all  metatdata
        for i, line in enumerate(self.content_ascii_lines):
            # meta data
            m = re.match(r"(^Acquisition Date and Time:\t)(.*)", line)
            if m is not None:
                self.metadata_.timestamp = datetime.strptime(
                    m[2], '%Y%m%d %H:%M:%S')

            self._search_metadata("meas_software_version", line,
                                  r"(^Version:\t)(.*)", 2)
            self._search_metadata("meas_procedure_name", line,
                                  r"(^Method:\t)(.*)", 2)

            self._search_metadata("barcode", line,
                                  r"(^Sample ID:\t)(.*)", 2)
            self._search_metadata("user", line,
                                  r"(^User Name:\t)(.*)", 2)

            # data relevant for parsing
            m = re.match(r"(^Vial Number:\t)(.*)", line)
            if m is not None:
                self.well_num = int(m[2])

            m = re.match(r"(X Axis Multiplier:)\s*(\d*,\d*).*", line)
            if m is not None:
                self.x_axis_mulitplier = float(m.group(2).replace(',', '.'))
                logging.debug(
                    f"x-mult found: {i}: {m.group(2)} - {self.x_axis_mulitplier}")

            m = re.match(r"(Y Axis Multiplier:)\s*(\d*,\d*).*", line)
            if m is not None:
                self.y_axis_mulitplier = float(m.group(2).replace(',', '.'))
                logging.debug(
                    f"y-mult found: {i}: {m.group(2)} - {self.y_axis_mulitplier}")

            #  data_start_line_label = 'xydata'
            m = re.match(r"(^Y Axis Multiplier)(.*)", line)
            if m is not None:
                logging.debug(f"start of data found: {i} ")
                self.data_start_line = i + 1

        # metadata not specified in file
        self.metadata_.method = 'HPLC'
        self.metadata_.session_name = 'NA'

        self.metadata_.device_type = 'HPLC'
        self.metadata_.device_name = 'Hitachi Chromaster'
        self.metadata_.device_serial = 'NA'
        self.metadata_.device_version = 'NA'

        self.metadata_.meas_software = 'Hitachi Chromaster'

        self.metadata_.geolocation = 'NA'
        self.metadata_.altitude = 'NA'

        self.metadata_.environment_temperature = 'NA'
        self.metadata_.environment_air_humidity = 'NA'
        self.metadata_.environment_air_pressure = 'NA'

    def _read_data(self):
        df_header = ['time', 'value']
        t_y_values = self._read_asc_data()
        self.dataframe_ = pd.DataFrame(t_y_values, columns=df_header)
        self.dataframe_['wavelength'] = np.nan
        self.dataframe_['col'] = np.nan
        self.dataframe_['row'] = np.nan
        self.dataframe_['row_num'] = np.nan
        self.dataframe_['col_num'] = np.nan
        self.dataframe_['well_num'] = self.well_num
        self.dataframe_['well_name'] = self.well_num
        self.dataframe_['sample_name'] = self.metadata.barcode
        self.dataframe_['sample_group'] = np.nan
        self.dataframe_['saturated'] = np.nan
        self.dataframe_['measurement_start'] = self.metadata_.timestamp
        self.dataframe_['measurement_duration'] = timedelta(
            seconds=self.dataframe_['time'].max())
        self.dataframe_['temperature'] = np.nan

    def _read_asc_data(self):
        t_y_values = []
        for i, line in enumerate(self.content_ascii_lines[self.data_start_line + 1:]):
            t = i * self.x_axis_mulitplier
            y = float(line) * self.y_axis_mulitplier
            t_y_values.append([t, y])
        return t_y_values
