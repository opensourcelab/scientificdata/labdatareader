"""_____________________________________________________________________

:PROJECT: LabDataReader

*spectrophotometer data descriptor*

:details: spectrophotometer data descriptor.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          220318

.. note:: -
.. todo:: -

________________________________________________________________________

"""

__version__ = "0.0.4"

import numpy as np

from labdatareader.data_descriptor import DataDescriptor
from labdatareader.data_descriptor import DescriptionFormat as desf
from labdatareader.metadata import Plot2D, Plot3D


class FluorescenceDataDescriptor(DataDescriptor):
    """The FluorescenceDataDescriptor is used to describe the columns of the resulting data frame
    """
    # sample type, sample group, sample ID
    rdf_header = """<!-- Spectrophotometer absorbance data description -->
< ?xml version = "1.0" encoding = "UTF-8"?>"""

    description_method_md = "Fluorescence Measurement - Thermo SkanIT 6"

    description_device_settings_md = """**Generic Device Settings**

Please adjust your device output according to the reader's data file parsing."""

    # standard items correspond to column names in data frame
    standard_items = {"value": {"markdown": "absorption value measured",
                                "type": float,
                                "quantity": "absorbance",
                                "default": np.nan,
                                "dimension": "/1",
                                "unit": "rel. AU",
                                "RDF-turtle": "LabDataReader:absorbance-value"},
                      "wavelength_ex": {"markdown": "fluorescence excitation wavelengths, column can contain multiple wavelengths",
                                     "type": int,
                                     "quantity": "wavelength",
                                     "default": np.nan,
                                     "dimension": "length",
                                     "unit": "nm",  # or better m ?
                                     "RDF-turtle": ""},
                      "wavelength_em": {"markdown": "fluorescence emission wavelengths, column can contain multiple wavelengths",
                                     "type": int,
                                     "quantity": "wavelength",
                                     "default": np.nan,
                                     "dimension": "length",
                                     "unit": "nm",  # or better m ?
                                     "RDF-turtle": ""},
                      "saturated": {"markdown": "states, whether detector was saturated during measurement ",
                                    "type": bool,
                                    "default": False,
                                    "RDF-turtle": "labData:sensor-saturated"},
                      "row": {"markdown": "row position of container for this measurement",
                              "type": str,
                              "default": "A",
                              "RDF-turtle": "LabDataReader:container-row"},
                      "col": {"markdown": "column of container",
                              "default": 1, },
                      "row_num": {"markdown": "row number of measured sample in container",
                                  "type": int,
                                  "default": 0, },
                      "col_num": {"markdown": "column number of measured sample in container",
                                  "type": int,
                                  "default": 0, },
                      "well_num": {"markdown": "standard well number, of container, starting from 0",
                                   "type": int,
                                   "default": 0, },
                      "well_name": {"markdown": "full well name, padded, e.g. A01, H12",
                                    "type": str,
                                    "default": "A01", },
                      "sample_name": {"markdown": "name of sample measured",
                                      "type": str,
                                      "default": "default name", },
                      "sample_group": {"markdown": "user defined group to which this sample belongs",
                                       "type": str,
                                       "default": "default group", },
                      "sample_type": {"markdown": "type (or role) of sample, e.g. negCtrl, posCtrl, blank, sample",
                                      "type": str,
                                      "default": "sample",
                                      "RDF-turtle": "LabDataReader:absorbance-value"},
                      "temperature": {"markdown": "current temperature of measurement",
                                      "type": float,
                                      "quantity": "temperature",
                                      "default": np.nan,
                                      "dimension": "temperature",
                                      "unit": "K",
                                      "RDF-turtle": "LabDataReader:absorbance-value"},
                      "measurement_start": {"markdown": "start of measurement for this sample",
                                            "default": np.nan,
                                            "unit": "s",
                                            "RDF-turtle": "LabDataReader:measurement-start"},
                      "measurement_duration": {"markdown": "duration of measurement for this sample",
                                               "quantity": "duration",
                                               "default": np.nan,
                                               "dimension": "time",
                                               "unit": "s",
                                               "RDF-turtle": "LabDataReader:measurement-start"},
                      "datetime": {"markdown": "absolute date and time when this sample was measured in ISO date format",
                                   "quantity": "date-time",
                                   "default": np.nan,
                                   "dimension": "time",
                                   "unit": "date-time",
                                   "RDF-turtle": "LabDataReader:datetime"},
                      "datetime_start": {"markdown": "absolute date and time when this sample was measured in ISO date format",
                                         "quantity": "date-time",
                                         "default": np.nan,
                                         "dimension": "time",
                                         "unit": "date-time",
                                         "RDF-turtle": "LabDataReader:datatime-start"}
                      }
    visualisation_default = Plot2D.XY
    visualisations = [Plot2D.XY, Plot2D.CHROMATOGRAM]

    def description(self, descr_format: desf = desf.MD):
        """
        docstring
        """
        if descr_format is desf.MD:
            return self.description_md
        else:
            raise NotImplementedError

    def description_method(self, descr_format: desf = desf.MD):
        """
        docstring
        """
        if descr_format is desf.MD:
            return self.description_method_md
        else:
            raise NotImplementedError

    def description_device_settings(self, descr_format: desf = desf.MD):
        """
        docstring
        """
        if descr_format is desf.MD:
            return self.description_device_settings_md
        else:
            raise NotImplementedError
