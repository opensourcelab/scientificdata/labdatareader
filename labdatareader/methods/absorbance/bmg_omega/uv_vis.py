"""_____________________________________________________________________

:PROJECT: LARAsuite

*BMG Omega plate reader UV-vis spectrometer data importer*

:details: BMG Omega plate reader UV-vis spectrometer data importer

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201216

.. note:: -
.. todo:: - test everything

________________________________________________________________________
"""


__version__ = "0.0.3"

import os
import sys
import csv
import re
import logging
from datetime import datetime, timedelta

import pandas as pd
import numpy as np

from labdatareader.metadata import MetaData
from labdatareader.data_reader_implementation import DataReaderImplementation

from labdatareader.methods.absorbance.absorbance_descriptor import AbsorbanceDataDescriptor


class DataReaderImpl(DataReaderImplementation):
    data_format_description = ("BMG Ohmega microtiter plate reader.\n"
                               "Absorbance measurements in the UV - vis spectroscopic range.\n"
                               "Single point as well as kinetic measurements can be read.\n")
    device_settings_description = """To reproduce the required data format, 
      please set up the device software with the following settings:

      **Date and Time Format**
        YYYY-MM--DD hh:mm:ss  (24hr format, UTC+0)

      **Floating Point Values**
        - floating point separator: '.'

      **Barcode**
        The barcode should be written into ID1

    """
    descriptor = AbsorbanceDataDescriptor()
    descriptor.description_md = data_format_description

    def __init__(self, data_path: str = ".",
                 data_filename_list: list = [],
                 header_map=None, metadata_defaults={}, data_defaults={}, data_start_ends=[(0, 0)]):
        super().__init__(data_path=data_path,
                         data_filename_list=data_filename_list,
                         header_map=header_map,
                         metadata_defaults=metadata_defaults,
                         data_defaults=data_defaults,
                         data_start_ends=data_start_ends)

        self.content_ascii_lines = None

    def _read_all_data(self):
        data_lst = []
        self._metadata = MetaData()
        self._metadata.meas_temperatures = []
        self._metadata.meas_filenames = self.data_filename_list
        self._metadata.wavelengths = []
        self._metadata.timestamps = []

        for session_num, data_filename in enumerate(self.data_filename_list):
            self._read_metadata(data_filename, session_num)
            self._read_data(data_lst, session_num)

        self._dataframe = pd.DataFrame(data_lst)
        self._dataframe["datetime_start"] = self._dataframe[
            "datetime"]  # pd.to_datetime(self._dataframe["datetime"])
        # reference time = smallest timepoint
        date_time_ref = self._dataframe["datetime_start"].min()

        self._dataframe["delta_datetime"] = self._dataframe[
            "datetime_start"] - date_time_ref
        # calculate time difference in seconds
        # relative to first measurement as reference, ignoring duration
        self._dataframe["delta_time_ref_s"] = self._dataframe["delta_datetime"].apply(
            lambda delta_datetime: delta_datetime.total_seconds())

    def _read_metadata(self, data_filename: str, session_num: int):
        last_line = False
        self.num_cycles = 0
        self.postioning_delay = 0.2

        self.content_ascii_lines = self._readlines_utf8(data_filename)

        # todo: handle first iteration separately

        # extract all  metatdata
        for i, line in enumerate(self.content_ascii_lines):
            if i == len(self.content_ascii_lines):
                last_line = True
            m = re.match(r"(^Date:)(.*)", line)
            if m is not None:
                if session_num == 1:
                    self._metadata.timestamp = datetime.strptime(
                        line.strip(), 'Date: %d/%m/%y  Time: %H:%M:%S %p')
                self._metadata.timestamps.append(datetime.strptime(
                    line.strip(), 'Date: %d/%m/%y  Time: %H:%M:%S %p'))
            else:
                self._metadata.timestamp = datetime.min

            # self.search_metadata("barcode", line,
            #                     r"(^ID1:\s*)(.*)ID2:\s(.*)ID3:\s(.*)", 2)
            self.search_metadata("meas_procedure_name", line, last_line,
                                 r"(^Testname:\s*)(.*)", 2)
            self.search_metadata("meas_software", line, last_line,
                                 r"(^Reader type:\s*)(.*)", 2)
            self.search_metadata("meas_software_version", line, last_line,
                                 r"(^Omega software version \(control part\):\s)(.*)", 2)
            self.search_metadata("device_version", line, last_line,
                                 r"(^Firmware version:\s*)(.*)", 2)
            self.search_metadata("device_serial", line, last_line,
                                 r"(^Reader serial number:\s*)(.*)", 2)
            self.search_metadata("users", line, last_line,
                                 r"(^BMG user:\s)(.*)", 2)

            # No. of Channels / Multichromatics:
            m = re.match(r"(^Positioning delay \[s\]:\s*)(\d*.\d*)(.*)", line)
            if m is not None:
                # might be necessary to store in list
                self.postioning_delay = float(m[2])

            # No. of Cycles:   - for kinetics in one file
            m = re.match(r"(^No\. of Cycles:\s*)(\d*)(.*)", line)
            if m is not None:
                # might be necessary to store in list
                self.num_cycles = int(m[2])

            m = re.match(r"(^\s*\d:\s*)(\d*)nm\s*(.*)", line)
            if m is not None:
                self._metadata.wavelengths.append(m[2])

            #  data_start_line_label = 'Chromatic'
            self._search_data_begin_end_lines(i, line, r"^Chromatic:\s*\d*", 4)

            # search current temperature
            m = re.match(r"(^T\[°C\]\s*:\s*)(\d*\.\d*)(.*)", line)
            if m is not None:
                self._metadata.meas_temperatures.append(m[2])
                logging.debug(f"temperature found: {m[2]} ")

        self._add_end_line(i)
        # metadata not specified in file
        self._metadata.method = 'absorbance'
        self._metadata.session_name = str(
            self._metadata.timestamp) + self._metadata.meas_procedure_name

        self._metadata.device_type = 'microtiter platereader'
        self._metadata.device_name = self._metadata.meas_software

        self.search_metadata("environment_temperature")
        self.search_metadata("environment_air_pressure")
        self.search_metadata("environment_air_humidity")
        self.search_metadata("geolocation")
        self.search_metadata("altitude")

    def _read_data(self, data_lst, session_num):
        # iterating through all measured wavelengths_list by begin-end lines array
        session_datetime = self._metadata.timestamps[session_num]

        for n_meass, begin_end in enumerate(self.data_begin_end_lines):
            wavelength = self._metadata.wavelengths[n_meass]
            measurement_duration = self.postioning_delay

            # just iterate over begin-end interval
            for i, line in enumerate(self.content_ascii_lines[begin_end[0]:]):
                if i + begin_end[0] == begin_end[1]:
                    break

                well_value = line.split()
                well_name = well_value[0]
                # to make it more robust for German decimal commas
                value = float(well_value[1].replace(',', '.'))

                curr_row, curr_col = self._std_well_name2row_col(well_name)
                curr_row_num, curr_col_num = self._std_well_name2row_col_num(
                    well_name)

                # see descriptor for all column names
                data_row = dict(row=curr_row,
                                col=curr_col,
                                row_num=curr_row_num,
                                col_num=curr_col_num,
                                well_num=self._std_well_name2well_num(
                                    well_name, 12),
                                well_name=well_name,
                                sample_name=np.nan,
                                sample_group=np.nan,
                                sample_type=np.nan,
                                wavelength=wavelength,
                                value=value,
                                saturated=False,
                                temperature=np.nan,
                                measurement_start=i * measurement_duration,
                                measurement_duration=measurement_duration,
                                datetime=session_datetime +
                                timedelta(seconds=i * measurement_duration)
                                )
                data_lst.append(data_row)
