"""_____________________________________________________________________

:PROJECT: LabDataReader

*spectrophotometer data descriptor*

:details: spectrophotometer data descriptor.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201023

.. note:: -
.. todo:: -

________________________________________________________________________

"""

__version__ = "0.0.4"

import numpy as np

from labdatareader.data_descriptor import DataDescriptor
from labdatareader.data_descriptor import DescriptionFormat as desf
from labdatareader.metadata import Plot2D, Plot3D


class AbsorbanceDataDescriptor(DataDescriptor):
    """The AbsorbanceDataDescriptor is used to describe the columns of the resulting data frame
    """
    # sample type, sample group, sample ID
    rdf_header = """@prefix osom: <http://oso.org/measurement#> ."""

    description_method_md = """The absorbance of a beam of collimated monochromatic radiation in a homogeneous isotropic medium is proportional to the 
                               absorption path length and to the concentration of the absorbing species. Absorbance is a logarithmic measure of the 
                               amount of light (at a specific wavelength) that is absorbed when passing through a sample.  
                               A= -log10(I/I0), where A is absorbance, I is the intensity of incident light and I0 is the intensity of light 
                               transmitted after passing through the sample."""

    description_device_settings_md = """**Generic Device Settings** 
     Please adjust your device output according to the reader's data file parsing."""  # describes, how the device output format should be set

    # standard items correspond to column names in data frame
    standard_items = {"value": {"markdown": "absorption value measured",
                                "type": float,
                                "quantity": "absorbance",
                                "default": np.nan,
                                "dimension": "/1",
                                "unit": "rel. AU",
                                "RDF-turtle": "osom:absorbance-value"},
                      "wavelength": {"markdown": "wavelength at which the absorption was measured, column can contain multiple wavelengths",
                                     "type": int,
                                     "quantity": "wavelength",
                                     "default": np.nan,
                                     "dimension": "length",
                                     "unit": "nm",  # or better m ?
                                     "RDF-turtle": "osom:absorbance-value"},
                      "saturated": {"markdown": "states, whether detector was saturated during measurement ",
                                    "type": bool,
                                    "default": False,
                                    "RDF-turtle": "labData:sensor-saturated"},
                      "row": {"markdown": "row position of container for this measurement",
                              "type": str,
                              "default": "A",
                              "RDF-turtle": "osom:container-row"},
                      "col": {"markdown": "column of container",
                              "default": 1, },
                      "row_num": {"markdown": "row number of measured sample in container",
                                  "type": int,
                                  "default": 0, },
                      "col_num": {"markdown": "column number of measured sample in container",
                                  "type": int,
                                  "default": 0, },
                      "well_num": {"markdown": "standard well number, of container, starting from 0",
                                   "type": int,
                                   "default": 0, },
                      "well_name": {"markdown": "full well name, padded, e.g. A01, H12",
                                    "type": str,
                                    "default": "A01", },
                      # "series_num": {"markdown": "number of a data set within a series of measurements, e.g. within a kinetic measurement",
                      #               "type": int,
                      #               "default": 0, },
                      "sample_name": {"markdown": "name of sample measured",
                                      "type": str,
                                      "default": "default name", },
                      "sample_group": {"markdown": "user defined group to which this sample belongs",
                                       "type": str,
                                       "default": "default group", },
                      "sample_type": {"markdown": "type (or role) of sample, e.g. negCtrl, posCtrl, blank, sample",
                                      "type": str,
                                      "default": "sample",
                                      "RDF-turtle": "osom:absorbance-value"},
                      "temperature": {"markdown": "current temperature of measurement",
                                      "type": float,
                                      "quantity": "temperature",
                                      "default": np.nan,
                                      "dimension": "temperature",
                                      "unit": "K",
                                      "RDF-turtle": "osom:absorbance-value"},
                      "measurement_start": {"markdown": "start of measurement for this sample",
                                            "default": np.nan,
                                            "unit": "s",
                                            "RDF-turtle": "osom:measurement-start"},
                      "measurement_duration": {"markdown": "duration of measurement for this sample",
                                               "quantity": "duration",
                                               "default": np.nan,
                                               "dimension": "time",
                                               "unit": "s",
                                               "RDF-turtle": "osom:measurement-start"},
                      "datetime": {"markdown": "absolute date and time when this sample was measured in ISO date format",
                                   "quantity": "date-time",
                                   "default": np.nan,
                                   "dimension": "time",
                                   "unit": "date-time",
                                   "RDF-turtle": "osom:datetime"},
                      "datetime_start": {"markdown": "absolute date and time when this sample was measured in ISO date format",
                                         "quantity": "date-time",
                                         "default": np.nan,
                                         "dimension": "time",
                                         "unit": "date-time",
                                         "RDF-turtle": "osom:datatime-start"}
                      }
    visualisation_default = Plot2D.XY
    visualisations = [Plot2D.XY, Plot2D.CHROMATOGRAM]

    def description(self, descr_format: desf = desf.MD):
        """
        docstring
        """
        if descr_format is desf.MD:
            return self.description_md
        else:
            raise NotImplementedError

    def description_method(self, descr_format: desf = desf.MD):
        """
        docstring
        """
        if descr_format is desf.MD:
            return self.description_method_md
        else:
            raise NotImplementedError

    def description_device_settings(self, descr_format: desf = desf.MD):
        """
        docstring
        """
        if descr_format is desf.MD:
            return self.description_device_settings_md
        else:
            raise NotImplementedError
