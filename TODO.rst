LabDataReader To-DOs
=======================

- jasco spectrum
- Chromaster HPLC
- SkanIT6 spectrum
- Skanit6 fluorescense
- SkanIT6 luminiscense
- BMG spectrum
- Skanit2 absorbance
- Skanit2 fluorescense
- Shimadzu absorbance
- Shimadzu EDLS


- multi list:
  - try to make a property 
  - add remaing unittests
- modify and add all reader / unittests

- compound file types (like HPLC, FPLC, GC-MS)
  - check how to handle in separate readers


Metadata:
    - unified metadata - everything about a measurement should be in metadata
    - container geommetry, sample 
    - order of wells measured / order of measurement
    - labware: o UUID
            o name 
            o prod. ID
    - ORCHID
    - user acronym
    - namespace
    - Sample ID

    - process that generated the sample (UUID, name)
    - dimension, units of columns
    - geolocation servic: address -> coord, altitude
    - weather service: location -> air pressure, env. temperature

- generic CSV reader
   * test list columns based on descriptor
   * test empty lines -> NaN
   * readers for
     - fluorescense
     - luminiscense
     - GC
     - electroporesis
     - 
     - (IR, NMR, MS, conductivity, pH, polariometry, CHN, )


Units in all data:
 - check sympy: https://docs.sympy.org/latest/modules/physics/units/dimensions.html


- HPLC: add descriptor

- reactivate all tests 


- data_reader: pass options to json and csv methods
- factory paradigm

- descriptor (human / dictionary / RDF)


- absorption data vs spectrum
- time series
- add fluorescense support 

- simulator